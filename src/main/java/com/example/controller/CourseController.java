package com.example.controller;

import com.example.courses.CourseService;
import com.example.demo.domain.Topic;
import com.example.courses.domain.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CourseController {
	@Autowired
	private CourseService courseService;

	//GET
	@RequestMapping("/topics/{id}/courses")
	public List<Course> getallcourses(@PathVariable String id){
		return courseService.getallcourses(id);
	}

	//GET
		@RequestMapping("/topics/{topicId}/courses/{id}")
	public Course getcourse(@PathVariable String id)
	{
		return courseService.getcourse(id);
	}

	//POST(create)
	@RequestMapping(method=RequestMethod.POST, value="/topics/{topicId}/courses")
	public void addCourse(@RequestBody Course course, @PathVariable String topicId)
	{
		course.setTopic(new Topic(topicId,"",""));
		courseService.addCourse(course);
	}

	//PUT(update)
	@RequestMapping(method = RequestMethod.PUT, value = "/topics/{topicId}/courses/{id}")
	public void updateCourse(@PathVariable String id, @RequestBody Course course, @PathVariable String topicId){
		course.setTopic(new Topic(topicId,"",""));
		courseService.updateCourse(course);
	}

	//DELETE(delete)
	@RequestMapping(method = RequestMethod.DELETE, value = "/topics/{topicId}/courses/{id}")
	public void deleteCourse(@PathVariable String id)
	{
		courseService.deleteCourse(id);
	}


}
