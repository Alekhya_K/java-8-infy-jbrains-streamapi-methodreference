package com.hackerrank;



import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import com.hackerrank.Category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class ContentService {

    //put your code here.
  List<Course> courseList = new ArrayList<>(
          Arrays.asList(

                  new Course(201, "subcourse1", 180, 10, 101),
                  new Course(202, "subcourse2", 120, 12, 101),
                  new Course(203, "subcourse3", 130, 13, 101)
          )
    );
    List<Course> courseList1 = new ArrayList<>(
            Arrays.asList(

                    new Course(201, "subcourse1", 180, 10, 102),
                    new Course(202, "subcourse2", 120, 12, 102),
                    new Course(203, "subcourse3", 130, 13, 102)
            )
    );
    List<Course> courseList2 = new ArrayList<>(
            Arrays.asList(

                    new Course(201, "subcourse1", 180, 10, 103),
                    new Course(202, "subcourse2", 120, 12, 103),
                    new Course(203, "subcourse3", 130, 13, 103)
            )
    );
    List<Category> CategoryList = new ArrayList<>(
        Arrays.asList(
                new Category(101,"CloudComputing","good",courseList ),
                new Category(102,"Machine Learning","bad",courseList1),
                new Category(103,"Springboot","avg",courseList2)
        )
    );

    public List<Category> getAllContent() {
        return CategoryList;

    }



}
