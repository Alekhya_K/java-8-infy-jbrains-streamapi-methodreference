package com.hackerrank.springboothandson1;


import java.util.*;


import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    Employee e1 = new Employee("Sandhya", 20, 0);
    Employee e2 = new Employee("Kemp", 24, 2);
    Employee e3 = new Employee("Anil", 22, 3);
    Employee e4 = new Employee("Kumar", 30, 6);
    Employee e5 = new Employee("Tim", 32, 7);

    public List<Employee> getEmployees() {
        //put your code here.
        List<Employee> eList = new ArrayList<>();
        eList.add(e1);
        eList.add(e2);
        eList.add(e3);
        eList.add(e4);
        eList.add(e5);
      //  Comparator<Employee> c = Comparator.comparing(Employee::getAge);

       Collections.sort(eList);

        return eList;

    }




}
