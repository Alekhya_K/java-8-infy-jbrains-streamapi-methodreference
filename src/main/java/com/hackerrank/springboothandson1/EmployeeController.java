package com.hackerrank.springboothandson1;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    //Put your code here
    @Autowired
    EmployeeService es;

    @RequestMapping(value = "/")
    public List<Employee> getEmpList(){
        //Put your code here
        List<Employee> list = es.getEmployees();
        return list;
    }

}
