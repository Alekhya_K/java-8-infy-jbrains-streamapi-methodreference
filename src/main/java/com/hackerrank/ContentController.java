package com.hackerrank;

import java.util.List;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class ContentController {
    //Put your code here.
    @Autowired
    ContentService contentservice;

    @RequestMapping("/")
    public List<Category> getAllContent(){
        return contentservice.getAllContent();
    }
}
