package com.frescofullstackapi;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;




@RestController
@RequestMapping("/test/")
public class HospitalController {

    @Autowired
    private HospitalService hospitalService;


    @RequestMapping(value = "/hospitals/{id}", method = RequestMethod.GET)
    public @ResponseBody Hospital getHospital(@PathVariable("id") int id) throws Exception {

        return hospitalService.getHospital(id);
    }

    @RequestMapping(value = "/hospitals", method = RequestMethod.GET)
    public @ResponseBody List<Hospital> getAllHospitals() throws Exception {
        return hospitalService.getAllHospitals();
    }


    @RequestMapping(method=RequestMethod.POST, value="/hospitals")
    public void addHospital(@RequestBody Hospital hospital ) {

            hospitalService.addHospital(hospital);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/hospitals/{id}")
    public void updateHospital(@RequestBody Hospital hospital) {

        hospitalService.updateHospital(hospital);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/hospitals/{id}")
    public void deleteHospital(@RequestBody Hospital hospital) {

        hospitalService.deleteHospital(hospital);
    }

}
