package com.frescofullstackapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FullStackApi {

    public static void main(String[] args) {
        SpringApplication.run(FullStackApi.class,args);
    }
}
