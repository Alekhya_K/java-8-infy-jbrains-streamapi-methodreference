package com.frescofullstackapi;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/*import com.example.project.HospitalRepository;
import com.example.project.Hospital;*/



@Service
public class HospitalService {

    @Autowired
    private HospitalRepository hospitalRepository;


    public List<Hospital> getAllHospitals(){
        List<Hospital> hospitals = new ArrayList<>();
        hospitalRepository.findAll().forEach(hospitals::add);
        return hospitals;
    }

    public Hospital getHospital(int id){
        return hospitalRepository.findById(id).orElse(null);
    }

    public void addHospital(Hospital hospital){

        hospitalRepository.save(hospital);
    }

    public void updateHospital(Hospital hospital){
        hospitalRepository.save(hospital);

    }

    public void deleteHospital(Hospital hospital) {

        hospitalRepository.delete(hospital);
    }
}
