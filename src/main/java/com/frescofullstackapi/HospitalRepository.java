package com.frescofullstackapi;


/*import com.example.project.Hospital;*/
import org.springframework.data.repository.CrudRepository;

public interface HospitalRepository extends CrudRepository<Hospital,Integer> {

}
