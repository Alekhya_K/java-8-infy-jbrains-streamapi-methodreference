package java8.java8annotations;

import java.lang.annotation.*;

//Marker Annotation - Annotation with no variables or value in it
//Single value Annotation - With single value
//Multivalued Annotation
@Target(ElementType.TYPE) //target is class
@Retention(RetentionPolicy.RUNTIME) //till runtime
@interface SmartPhone{

    String os();
    int version();
    int price() default 20000; //no need to  specify in annotation
}

@SmartPhone(os="Android", version = 6)
class NokiaASeries{
    public String modelName;
    int screenSize;

    public NokiaASeries(String modelName, int screenSize) {
        this.modelName = modelName;
        this.screenSize = screenSize;
    }
}
public class java8annotationDemo {
    public static void main(String[] args) {

        NokiaASeries obj = new NokiaASeries("fire",5);

        //To print values of annotation
        //Use Reflection Api

        //Step 1 - Create a class obj
        Class c =obj.getClass();
        Annotation an = c.getAnnotation(SmartPhone.class);
        SmartPhone s = (SmartPhone) an;
        System.out.println(s.os());


    }
}
