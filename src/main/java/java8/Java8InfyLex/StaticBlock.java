package java8.Java8InfyLex;

 class Loan {
     //loan counter counts the total no of objects
     private static int loanCounter;
     private int loanNo;
     private int accountNo;
     private int customerNo;
     private float loanAmount;
     private int loanDuration;
     private float interest;

     Loan(){
         //to count the total no of instances
         loanCounter++;
     }

     public Loan(int accountNo, int customerNo, float loanAmount, int loanDuration, float interest) {
         this.accountNo = accountNo;
         this.customerNo = customerNo;
         this.loanAmount = loanAmount;
         this.loanDuration = loanDuration;
         this.interest = interest;
         //to count the total no of instances
         loanCounter++;
     }

     public int getLoanNo() {
         return loanNo;
     }

     public void setLoanNo(int loanNo) {
         this.loanNo = loanNo;
     }

     public int getAccountNo() {
         return accountNo;
     }

     public void setAccountNo(int accountNo) {
         this.accountNo = accountNo;
     }

     public int getCustomerNo() {
         return customerNo;
     }

     public void setCustomerNo(int customerNo) {
         this.customerNo = customerNo;
     }

     public float getLoanAmount() {
         return loanAmount;
     }

     public void setLoanAmount(float loanAmount) {
         this.loanAmount = loanAmount;
     }

     public int getLoanDuration() {
         return loanDuration;
     }

     public void setLoanDuration(int loanDuration) {
         this.loanDuration = loanDuration;
     }

     public float getInterest() {
         return interest;
     }

     public void setInterest(float interest) {
         this.interest = interest;
     }

     public static int getLoanCounter() {
         return loanCounter;
     }
 }
 class LoanTester{
    public static void main (String[] args){

        //Create 2 objects of Loan class using the default and 2 using parametrized constructor.
        //Use getter methods and display the value of loanCounter for every instance
        Loan loan1 = new Loan();
        System.out.println(loan1.getLoanCounter());
        Loan loan = new Loan(1,1,1,1,1);
        System.out.println(Loan.getLoanCounter());

    }
}

