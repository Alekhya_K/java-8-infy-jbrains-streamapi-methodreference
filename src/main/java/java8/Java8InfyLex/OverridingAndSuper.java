package java8.Java8InfyLex;

 class EventRegistration {
     private String name;
     private String nameOfEvent;
     private double registrationFee;
     public EventRegistration(){

     }

     public EventRegistration(String name, String nameOfEvent) {
         this.name = name;
         this.nameOfEvent = nameOfEvent;
     }

     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }

     public String getNameOfEvent() {
         return nameOfEvent;
     }

     public void setNameOfEvent(String nameOfEvent) {
         this.nameOfEvent = nameOfEvent;
     }

     public double getRegistrationFee() {
         return registrationFee;
     }

     public void setRegistrationFee(double registrationFee) {
         this.registrationFee = registrationFee;
     }
     public void registerEvent(){

         switch (nameOfEvent) {
             case "ShakeALeg":
                 registrationFee = 100;
                 break;
             case "Sing&Win":
                 registrationFee = 150;
                 break;
             case "Actathon":
                 registrationFee = 70;
                 break;
             case "PlayAway":
                 registrationFee = 130;
                 break;
         }
     }
 }
  class SingleEventRegistration extends EventRegistration{
     private int participantNo;

      public SingleEventRegistration(String name, String nameOfEvent, int participantNo) {
          super(name, nameOfEvent);
          this.participantNo = participantNo;
      }

      public int getParticipantNo() {
          return participantNo;
      }

      public void setParticipantNo(int participantNo) {
          this.participantNo = participantNo;
      }
      //overriden method
      public void registerEvent(){
          switch (super.getNameOfEvent()) {
              case "ShakeALeg":
                  super.setRegistrationFee(100);
                  break;
              case "Sing&Win":
                  super.setRegistrationFee(150);
                  break;
              case "PlayAway":
                  super.setRegistrationFee(130);
                  break;
          }
          System.out.println("Your registration fee is:" +getRegistrationFee());
          System.out.println("The participant no is" +participantNo);
      }
  }
  class TeamEventRegistration extends EventRegistration{
     private int noOfParticipants;
     private int teamNo;

      public TeamEventRegistration(String name, String nameOfEvent, int teamNo, int noOfParticipants) {
          super(name, nameOfEvent);
          this.noOfParticipants = noOfParticipants;
          this.teamNo = teamNo;
      }

      public int getNoOfParticipants() {
          return noOfParticipants;
      }

      public void setNoOfParticipants(int noOfParticipants) {
          this.noOfParticipants = noOfParticipants;
      }

      public int getTeamNo() {
          return teamNo;
      }

      public void setTeamNo(int teamNo) {
          this.teamNo = teamNo;
      }
      //overriden method
      public void registerEvent(){
          switch (super.getNameOfEvent()) {
              case "ShakeALeg":
                  super.setRegistrationFee(50 * noOfParticipants);
                  break;
              case "Sing&Win":
                  super.setRegistrationFee(60 * noOfParticipants);
                  break;
              case "Actathon":
                  super.setRegistrationFee(80 * noOfParticipants);
              case "PlayAway":
                  super.setRegistrationFee(100 * noOfParticipants);
                  break;
          }
          System.out.println("Your registration fee is:" +getRegistrationFee());
          System.out.println("The participant no is" +teamNo);
      }
  }
 class ShowYourTalentRegistration{
    public static void main(String[] args){
        //TO-DO :Create an instance of SingleEventRegistration with reference of EventRegistration
        //as participant1, and initialize the same with values given
        EventRegistration participant1 = null;
        participant1 = new SingleEventRegistration("Jenny", "Sing&Win", 1);
        //TO-DO :Invoke the method registerEvent using participant1
        participant1.registerEvent();
        //TO-DO :Create an instance of TeamEventRegistration with reference of EventRegistration
        //as team1, and initialize the same with values given
        EventRegistration team1 = null;
        team1 = new TeamEventRegistration("Aura", "x", 1, 5);
        //TO-DO :Invoke the method registerEvent using team1
        team1.registerEvent();
        //TO-DO :Create an instance of SingleEventRegistration with reference of EventRegistration
        //as participant2, and initialize the same with values given
        EventRegistration participant2 = null;
        participant2 = new SingleEventRegistration("Hudson", "PlayAway", 2);
        //TO-DO :Invoke the method registerEvent using participant2
        participant2.registerEvent();
    }
}
