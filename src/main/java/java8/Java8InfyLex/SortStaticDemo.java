package java8.Java8InfyLex;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SortStaticDemo {
    public static void main(String[] args) {
        List<String> countrylst = Arrays.asList("India", "America", "Japan", "Brazil");
        countrylst.sort(Comparator.naturalOrder()); // will sort in String natural sorting order
        for (String countryName : countrylst) {
            System.out.println(countryName);
        }
    }
}

