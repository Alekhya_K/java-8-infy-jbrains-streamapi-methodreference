package java8.Java8InfyLex;

interface Greeting{
    default void hello() {
        System.out.println(" hello from Greeting");
    }
}

interface GreetingExtn extends Greeting{
     default void hello() {
	System.out.println(" hello from GreetingExtn");
     }
}

class Greet {
    //comment and uncomment this class to try more possibilities
        public void hello() {
        System.out.println("hello from Greet class");

    }
}

class DefaultStarter extends Greet implements Greeting, GreetingExtn{
    public static void main(String[] args) {
        DefaultStarter d = new DefaultStarter();
        d.hello();
    }
}
