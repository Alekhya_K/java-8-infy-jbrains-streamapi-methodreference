package java8.Java8InfyLex;

class PlayerRating{
    private int playerPosition;
    private String playerName;
    private float criticOneRating;
    private float criticTwoRating;
    private float criticThreeRating;
    private float avergaeRating;
    private char category;

    public PlayerRating(int playerPosition, String playerName) {
        this.playerPosition = playerPosition;
        this.playerName = playerName;
    }
    public void calculateAverageRating(float criticOneRating, float criticTwoRating){
        avergaeRating = (criticOneRating + criticTwoRating)/2;

    }
    public void calculateAverageRating(float criticOneRating, float criticTwoRating, float criticThreeRating){
        avergaeRating = (criticOneRating + criticTwoRating + criticThreeRating)/3;
    }
    public void calculateCategory(){
        if(avergaeRating>8){
            category = 'A';
        }
        else if(avergaeRating>5&&avergaeRating<=8){
            category = 'B';
        }
        else if(avergaeRating>0&&avergaeRating<=5){
            category = 'C';
        }
    }
    public void display(){
        System.out.println("The player name is :" +playerName);
        System.out.println("the player position is :" +playerPosition);
        System.out.println("the average rating is: " +avergaeRating);
        System.out.println("the category is: " +category);
    }

    public static void main(String[] args) {
        PlayerRating playerRating = new PlayerRating(1,"Oscar");
        playerRating.calculateAverageRating(9,(float)9.9);
        playerRating.calculateCategory();
        playerRating.display();
    }
}