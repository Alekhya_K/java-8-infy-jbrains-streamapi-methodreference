package java8.Java8InfyLex;

 class Registration {
     private String customerName;
     private String panCardNo;
     private int voterId;
     private String passportNo;
     private int licenseNo;
     private long[] telephoneNo;

     public Registration(String customerName, String passportNo, long[] telephoneNo) {
         this.customerName = customerName;
         this.passportNo = passportNo;
         this.telephoneNo = telephoneNo;
     }

     public Registration(String customerName, String panCardNo, int licenseNo, long[] telephoneNo) {
         this.customerName = customerName;
         this.panCardNo = panCardNo;
         this.licenseNo = licenseNo;
         this.telephoneNo = telephoneNo;
     }

     public Registration(String customerName, int voterId, int licenseNo, long[] telephoneNo) {
         this.customerName = customerName;
         this.voterId = voterId;
         this.licenseNo = licenseNo;
         this.telephoneNo = telephoneNo;
     }
     public Registration(String customerName,  int voterId, String panCardNo, long[] telephoneNo){
         this.customerName = customerName;
         this.voterId = voterId;
         this.panCardNo = panCardNo;
         this.telephoneNo = telephoneNo;
     }

     public String getCustomerName() {
         return customerName;
     }

     public String getPanCardNo() {
         return panCardNo;
     }

     public int getVoterId() {
         return voterId;
     }

     public String getPassportNo() {
         return passportNo;
     }

     public int getLicenseNo() {
         return licenseNo;
     }

     public long[] getTelephoneNo() {
         return telephoneNo;
     }

     public void setTelephoneNo(long[] telephoneNo) {
         this.telephoneNo = telephoneNo;
     }

     public static void main(String[] args) {

         Registration registration = new Registration("Kevin","sdfsf23", new long[]{23453534542L, 2334545354L});
        System.out.println(registration.getCustomerName());
         System.out.println(registration.getPassportNo());
         System.out.println(registration.getTelephoneNo());
     }

 }
