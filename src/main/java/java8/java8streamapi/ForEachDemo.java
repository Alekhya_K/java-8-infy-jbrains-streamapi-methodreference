package java8.java8streamapi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

//refer youtube tutorial java8 stream api on JavaTechie youtube channel
public class ForEachDemo {

   public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(3);
        list.add(4);
        //traditional approach for iteration
        for(int i:list){
            System.out.println(i);
        }
        list.stream().forEach(System.out::println);

        Map<Integer, String> map= new HashMap<>();
        map.put(1,"allu");
        map.put(2,"akshy");
        //method 1
        map.forEach((key,value)->System.out.println(key +":"+value));
        //method 2 using stream
        map.entrySet().stream().forEach(obj->System.out.println(obj));

        Consumer<Integer> consumer = t->System.out.println(t);
        consumer.accept(1);

        for (Integer s:
             list) {
            consumer.accept(s);
        }



    }
}
