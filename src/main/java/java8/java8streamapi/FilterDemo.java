package java8.java8streamapi;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FilterDemo {

    //filter ==> conditional check

    public static void main(String[] args) {
        List<String> list = Arrays.asList("allu","aksy","jo","da");
       //traditional approach
        for (String s:
             list) {
            if(s.startsWith("a")){
                System.out.println(s);
            }
        }
        //using filter method
        list.stream().filter(s -> s.startsWith("a")).forEach(System.out::println);

        //Map example
        Map<Integer, String> map = new HashMap<>();
        map.put(1,"allu");
        map.put(2,"akshu");
        map.put(3,"daddy");
        map.put(4,"jyothi");

        map.entrySet().stream().filter(k->k.getKey()%2==0).forEach(System.out::println);
    }
}
