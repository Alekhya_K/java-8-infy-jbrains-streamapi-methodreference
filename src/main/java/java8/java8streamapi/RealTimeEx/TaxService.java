package java8.java8streamapi.RealTimeEx;

import java.util.List;
import java.util.stream.Collectors;


public class TaxService {
    public static List<Employee> getTaxlogic(String s){

       return s.equalsIgnoreCase("tax") ?
             Database.getEmployee().stream().filter(t -> t.getSal() > 50000).collect(Collectors.toList()):

             Database.getEmployee().stream().filter(t -> t.getSal() <= 50000).collect(Collectors.toList());
    }



    public static void main(String[] args) {
        System.out.println(getTaxlogic("tax"));
        System.out.println(getTaxlogic("non tax"));
    }


}
