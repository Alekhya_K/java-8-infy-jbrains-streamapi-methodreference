package java8.java8streamapi.RealTimeEx;


import java.util.Arrays;
import java.util.List;

//hardcoded for example
public class Database {
    public static List<Employee> getEmployee(){
        List<Employee> list = Arrays.asList(
                new Employee(1,"allu",25000),
                new Employee(2,"akshy",60000),
                new Employee(3,"daddy",40000),
                new Employee(4,"jo",70000)
        );
        return list;
    }
}
