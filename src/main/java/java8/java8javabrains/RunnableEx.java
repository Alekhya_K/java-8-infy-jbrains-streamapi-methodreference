package java8.java8javabrains;

public class RunnableEx {
    public static void main(String[] args) {
        Thread mythread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Printed inside runnable");
            }
        });
        mythread.run();

        Thread mylambdathread = new Thread(()->System.out.println("Printed inside runnable"));
        mylambdathread.run();
    }
}
