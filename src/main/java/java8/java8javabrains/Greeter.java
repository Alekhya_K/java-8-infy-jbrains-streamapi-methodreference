package java8.java8javabrains;


public class Greeter {
    public void greet(Greeeting greeeting)
    {
        greeeting.perform();
    }

    public static void main(String args[])
    {
        Greeter greeter = new Greeter();
            Greeeting helloWorldGreeting = new HelloWorldGreeting();
            greeter.greet(helloWorldGreeting);

            //inner class implementing interface - inturn another method for implementing lambda - annonymous inner class
        Greeeting innerClassGreeting = new Greeeting() {
            @Override
            public void perform() {
                System.out.println("hiiii from inner class greeting");
            }
        };

        //lambda
        Greeeting mylambda = ()-> System.out.println("hiiii from lambda expression");
        mylambda.perform();
    }
}


