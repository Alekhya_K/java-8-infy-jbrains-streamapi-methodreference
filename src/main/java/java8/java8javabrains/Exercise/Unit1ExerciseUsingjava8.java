package java8.java8javabrains.Exercise;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Unit1ExerciseUsingjava8 {
    public static void main(String[] args) {
        List<Person> personList = Arrays.asList(
                new Person("Alekhya","b", 23),
                new Person("Akshara", "Cab", 24),
                new Person("Damu","Ck", 50),
                new Person("Jyothi", "kd", 45)
        );

        //Step1 - Sort the list by lname

        Collections.sort(personList,(s1, s2)->s1.getLastName().compareTo(s2.getLastName()));
        System.out.println(personList);

        //Step2 - create a method that prints all elements in the list.

        PrintConditionally(personList, p->true, p -> System.out.println(p));

        //Step3 - Create a method that prints all people that have a lastname beginning with c

        PrintConditionally(personList, p-> p.getLastName().startsWith("C"),  p -> System.out.println(p));

    }

    //personPredicate (Predicate<T>) - Is a functional interface which can be found in java.util.functions - If
    //it is already present with the signature we need then we can use that specific interface
    //Consumer<T> - takes inuput object and returns void


    private static void PrintConditionally(List<Person> personList, Predicate<Person> personPredicate, Consumer<Person> consumer) {
        for (Person person : personList) {
            if(personPredicate.test(person))
            {
                consumer.accept(person);
            }

        }

    }
}
