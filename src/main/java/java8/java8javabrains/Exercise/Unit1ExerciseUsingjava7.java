package java8.java8javabrains.Exercise;

import java.util.*;

public class Unit1ExerciseUsingjava7 {
    public static void main(String[] args) {

        List<Person> personList = Arrays.asList(
                new Person("Alekhya","b", 23),
                new Person("Akshara", "Cab", 24),
                new Person("Damu","Ck", 50),
                new Person("Jyothi", "kd", 45)
        );

        //Step1 - Sort the list by lname

        //Step2 - create a method that prints all elements in the list.

        //Step3 - Create a method that prints all people that have a lastname beginning with c

        //Step1
        Collections.sort(personList, new Comparator<Person>() {
            @Override
            public int compare(Person person, Person t1) {
               return person.getLastName().compareTo(t1.getLastName());
            }
        });

        printList(personList);

        //Printing persons whose last name startswith c
        PrintSome(personList, new Condition() {
            @Override
            public boolean test(Person p) {
                    return p.getLastName().startsWith("C");
            }
        });
    }

    private static void PrintSome(List<Person> personList, Condition condition) {
             for (Person person : personList) {
                 if(condition.test(person))
                 {
                     System.out.println(person);
                 }

             }

        }

    private static void printList(List<Person> personList) {
        for (Person p:
             personList) {
            System.out.println(p);
        }
    }

}

interface Condition{
     boolean test(Person p);
}