package java8.java8datetimeapi;


import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class DateTimeDemo {


    public static void main(String[] args) {
/*        LocalDate d= LocalDate.of(1998, Month.OCTOBER, 5);//.now() method is used to print date
        System.out.println(d);
        for(String s:ZoneId.getAvailableZoneIds()){
            System.out.println(s);
        }

       LocalTime t =LocalTime.now(ZoneId.of("Asia/Aden"));
        System.out.println(t);
        */
        //examples of Local date - refer java8 date api tut on javatpoint

        LocalDate localDate = LocalDate.now();
        LocalDate yesterday  = localDate.minusDays(1);
        LocalDate tomorrow = yesterday.plusDays(2);
        System.out.println("todays date is:"+localDate+"yesterdays date is:"+yesterday+"tomorrows date is:"+tomorrow);

        //LocalDate example - isLeapYear()
        System.out.println(localDate.isLeapYear());

        //Java LocalDate Example: atTime() - print the date along with the specified time
        System.out.println(yesterday.atTime(1,50));

        //Local time
        LocalTime t = LocalTime.now();
        System.out.println(t);

        //Java LocalTime Example: minusHours() and minusMinutes() and plusHours() and plusMinutes()
        System.out.println(t.minusHours(5));

        //Local time in different regions around the world
        //System.out.println(ZoneId.getAvailableZoneIds());

        ZoneId zone1 = ZoneId.of("Asia/Kolkata");
        ZoneId zone2 = ZoneId.of("Singapore");
        LocalTime time1 = LocalTime.now(zone1);
        LocalTime time2 = LocalTime.now(zone2);
        System.out.println("Local time in zone1 :"+time1);
        System.out.println("Local time in zone2 :"+time2);
        long hrsdifference = ChronoUnit.HOURS.between(time1,time2);
        System.out.println("Differene between two time zones is :"+hrsdifference);

        //Java LocalDateTime class
        //default format as yyyy-MM-dd-HH-mm-ss.zzz.

        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("Before formatting date and time :"+localDateTime);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formatDateTime = localDateTime.format(formatter);
        System.out.println("After formatting date and time:"+formatDateTime);


        //Java LocalDateTime Example: get() - used to get the day of week or day of year or day of month or hr of day
        LocalDateTime localDateTime1 = LocalDateTime.now();
        System.out.println(localDateTime1.get(ChronoField.DAY_OF_MONTH));

        //Java LocalDateTime Example: get()
        final Clock clock=Clock.systemUTC();
        System.out.println(clock.instant());
        System.out.println(clock.millis());

    }

}
