package java8.java8fucntionalinterface;

import java.util.function.Supplier;


//refer youtube tutorial java8 Functional interface on JavaTechie youtube channel
public class SupplierDemo{

/*

    @Override
    public Integer get() {
       return 3;
    }
*/

    public static void main(String[] args) {
        Supplier<Integer> supplier = () -> 3;

        System.out.println(supplier.get());
    }
}
