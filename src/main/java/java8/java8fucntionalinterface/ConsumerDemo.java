package java8.java8fucntionalinterface;

import java.util.Arrays;
import java.util.List;


//refer youtube tutorial java8 Functional interface on JavaTechie youtube channel
public class ConsumerDemo{

    public static void main(String[] args) {

/*
        Consumer<Integer> consumer = integer-> System.out.println("printing" +integer);
        consumer.accept(4);
*/

        List<Integer> list1 = Arrays.asList(1,2,3,4);
        list1.stream().forEach(integer-> System.out.println("printing" +integer));

    }
}
