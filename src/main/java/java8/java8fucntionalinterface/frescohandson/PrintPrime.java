package java8.java8fucntionalinterface.frescohandson;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class PrintPrime {

/*
    @Override
    public boolean test(Integer integer) {
        for(int i=2;i<integer;i++){
            if(integer%i==0)
                return false;
        }
        return true;
    }
*/

    public static void main(String[] args) {
        Predicate<Integer> predicate = (integer) -> {
            for(int i=2;i<integer;i++){
                if(integer%i==0)
                    return false;
            }
            return true;
        };
        List<Integer> list = Arrays.asList(3,5,2,7,6);

        list.stream().filter(predicate).forEach(t->System.out.println(t));
    }
}
