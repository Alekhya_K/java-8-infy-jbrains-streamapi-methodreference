package java8.java8methodreference;

import java.util.Arrays;
import java.util.List;


//refer youtube tutorial java8 Method Reference on JavaTechie youtube channel
//using consumer interface
public class MethodReference {
    public static void main(String[] args) {
        List<Integer> list =Arrays.asList(1,2,3,4,5);
        list.forEach(p->MethodReference.print(p));//without method reference
        list.forEach(MethodReference::print);//with method reference
    }

    //similar as consumer(return type void and input arg generic type)
    public static void print(Integer i){
       System.out.println(i);
    }

}
