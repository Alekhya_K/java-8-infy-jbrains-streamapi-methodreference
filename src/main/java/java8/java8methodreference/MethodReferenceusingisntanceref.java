package java8.java8methodreference;

import java.util.Arrays;
import java.util.List;

//using consumer interface
public class MethodReferenceusingisntanceref {
    public static void main(String[] args) {
        MethodReferenceusingisntanceref ex = new MethodReferenceusingisntanceref();
        List<Integer> list = Arrays.asList(1,2,3,4,5);
        list.forEach(p->ex.print(p));//without method reference
        list.forEach(ex::print);//with method reference
    }

    //similar as consumer(return type void and input arg generic type)
    public void print(Integer i){
        System.out.println(i);
    }

}
